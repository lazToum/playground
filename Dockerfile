FROM python:3.9-slim as base

# add non-root user
ARG GROUP_ID
ARG USER_ID
ENV GROUP_ID=${GROUP_ID:-1000}
ENV USER_ID=${USER_ID:-1000}
RUN addgroup --system --gid ${GROUP_ID} runner
RUN adduser --disabled-login --disabled-password --system --uid ${USER_ID} --gid ${GROUP_ID} runner
RUN mkdir -p /home/runner/.local /home/runner/backend && \
    chown -R runner:runner /home/runner/.local /home/runner/backend
# envs
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    DEBIAN_FRONTEND=noninteractive \
    DEBCONF_NONINTERACTIVE_SEEN=true \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1\
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    SETUPTOOLS_USE_DISTUTILS=stdlib \
    POETRY_NO_INTERACTION=1 \
    POETRY_NO_ROOT=1 \
    POETRY_HOME=/home/runner/.local \
    POETRY_VIRTUALENVS_CREATE=false

ENV PATH=/home/runner/.local/bin:$POETRY_HOME/bin:$PATH

FROM base as build-phase
RUN apt update && \
    apt install -y --no-install-recommends \
    curl \
    gcc \
    build-essential && \
    rm -rf /var/cache && \
    rm -rf /var/lib/apt/lists/*

USER runner
RUN pip install --upgrade pip setuptools wheel
RUN curl -sSL https://install.python-poetry.org | python3 -
COPY ./poetry.lock ./pyproject.toml ./
ARG BACKEND_ENV=production
ENV BACKEND_ENV=$BACKEND_ENV
RUN  bash -c "if [ "${BACKEND_ENV}" = 'production' ]; then \
    poetry install --no-dev; else \
    poetry install; \
    fi"

FROM base
USER runner
WORKDIR /home/runner/backend
COPY --from=build-phase --chown=${USER_ID}:${GROUP_ID} /home/runner/.local /home/runner/.local
COPY --chown=${USER_ID}:${GROUP_ID} . .

ARG BACKEND_PORT=5000
ENV BACKEND_PORT=$BACKEND_PORT
EXPOSE $BACKEND_PORT
CMD ["python3", "-m", "app"]
