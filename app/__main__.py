"""-*- coding: utf-8 -*-."""
import os
from typing import Any, Dict

from fastapi import APIRouter, FastAPI
from fastapi.responses import ORJSONResponse
from uvicorn import run  # type: ignore

try:
    from app.version import __version__
except ImportError:
    import sys

    sys.path.insert(0, os.path.realpath(os.path.join(os.path.dirname(__file__), "..")))
    from app.version import __version__

app = FastAPI(
    title="Test",
    description="Dummy",
    docs_url=None,
    redoc_url=None,
    version=__version__,
    default_response_class=ORJSONResponse,
)

main_router = APIRouter(
    prefix="/api",
)


@main_router.get("/health", tags=["Healthcheck"])
async def health() -> Dict[str, Any]:
    """ "Healthcheck."""
    return {"status": "ok"}


app.include_router(main_router)


if __name__ == "__main__":
    _base = os.path.basename(os.path.dirname(__file__))
    _name = os.path.splitext(os.path.basename(__file__))[0]
    run(
        f"{_base}.{_name}:app",
        reload=False,
        host="0.0.0.0",
        port=5000,
        loop="uvloop",
        server_header=False,
        date_header=False,
    )
