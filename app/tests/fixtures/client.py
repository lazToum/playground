"""-*- coding: utf-8 -*-."""
import os
from typing import AsyncGenerator

import pytest_asyncio
from asgi_lifespan import LifespanManager
from httpx import AsyncClient

try:  # pragma: no cover
    from app.__main__ import app
except ImportError:
    import sys

    sys.path.insert(
        0, os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "..", ".."))
    )
    from app.__main__ import app


@pytest_asyncio.fixture(scope="session", autouse=True)
async def async_client() -> AsyncGenerator:
    """Yield async http client."""
    scheme = "http"
    async with AsyncClient(
        app=app, base_url=f"{scheme}://backend/api"
    ) as client, LifespanManager(app):

        yield client
