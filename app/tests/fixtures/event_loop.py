"""-*- coding: utf-8 -*-."""
from asyncio import get_event_loop_policy
from typing import Generator

import pytest


@pytest.fixture(scope="session", autouse=True)
def event_loop() -> Generator:
    """Creates an instance of the default event loop for the test session."""
    loop = get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()
