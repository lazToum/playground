"""-*- coding: utf-8 -*-."""
import pytest
from fastapi import status
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_get_health(async_client: AsyncClient) -> None:
    """Test healthcheck endpoint."""
    response = await async_client.get("/health")
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {"status": "ok"}
