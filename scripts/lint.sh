#!/usr/bin/env sh
_HERE="$(dirname "$(readlink -f "$0")")"
if [ "${_HERE}" = "." ]; then
  _HERE="$(pwd)"
fi
_ROOT_DIR="$(dirname "${_HERE}")"
if [ "${_ROOT_DIR}" = "." ]; then
  _ROOT_DIR="$(pwd)"
fi
lint(){
  isort --check "${_ROOT_DIR}/app"
  black --check "${_ROOT_DIR}/app"
  mypy "${_ROOT_DIR}/app"
  flake8 --config="${_ROOT_DIR}/.flake8"
}

format() {
  cd "${_ROOT_DIR}" || exit 1
  isort "${_ROOT_DIR}/app"
  autoflake --recursive --remove-unused-variables --in-place "${_ROOT_DIR}/app" --exclude=__init__.py
  black "${_ROOT_DIR}/app"
}

if ! [ "${1}" = "--fix" ];then
  set -e
  set -x
  lint
else
  lint
fi
if [ "${1}" = "--fix" ];then
  format
fi
bash "${_HERE}/pylint.sh"
