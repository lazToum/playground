#!/usr/bin/env sh
_ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"
if [ "${_ROOT_DIR}" = "." ]; then
  _ROOT_DIR="$(pwd)"
fi
cd "${_ROOT_DIR}" || exit 1
mkdir -p reports/badges reports/logs
pylint --rcfile=pyproject.toml --output-format=text app | tee reports/logs/pylint.log || pylint-exit $?
PYLINT_SCORE="$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' reports/logs/pylint.log)"
anybadge --label=Pylint --overwrite --file=reports/badges/pylint.svg --value=$PYLINT_SCORE 2=red 4=orange 6=teal 8=yellow 10=green
echo "Pylint score is $PYLINT_SCORE"
